def factors(number):
    # ==============
    # Your code here
	factors = []
	for i in range(2, number):
		ans = number % i
		if ans == 0:
			factors.append(i)
	if len(factors) == 0:
		prime = (str(number) + " is a prime number")
		return prime
	elif len(factors) > 1:
		return factors
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
