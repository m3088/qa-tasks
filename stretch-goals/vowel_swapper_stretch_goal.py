def vowel_swapper(string):
    # ==============
    # Your code here


	string_list = []
	string_lower = string.lower()

	for i in range(len(string)):
		ch = (string[i - 1])
		string_list.insert((i - 1), ch)


	try:
		a = string_lower.index("a", (string_lower.index("a") + 1))
		string_list[a] = "/\\"
	except:
		a = 0
	try:
		e = string_lower.index("e", (string_lower.index("e") + 1))
		string_list[e] = "3"
	except:
		e = 0
	try:
		i = string_lower.index("i", (string_lower.index("i") + 1))
		string_list[i] = "!"
	except:
		i = 0
	try:
		o = string_lower.index("o", (string_lower.index("o") + 1))
		if string_list[o] == "o":
			string_list[o] = "ooo"
		elif string_list[o] == "O":
			string_list[o] = "000"
		else:
			print("Error")
	except:
		o = 0
	try:
		u = string_lower.index("u", (string_lower.index("u") + 1))
		string_list[u] = "\\/"
	except:
		u = 0

	string_modified = "".join(string_list)
	return string_modified


    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console

