def calculator(a, b, operator):
    # ==============
    # Your code here
	if operator == "+":
		ans =  a + b
	elif operator == "-":
		ans = a - b
	elif operator == "*":
		ans = a * b
	elif operator == "/":
		ans = a / b
	else:
		print("Error")
	return format(int(ans), "b")
    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
